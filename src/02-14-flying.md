## Stop Flying

```admonish warning
This is probably our most controversial suggestion for a big win.
```

The sailing channel who recently shared that to get to a destination involved 12 flights won't like this.

Flying has the largest carbon footprint of all modes of transport and, despite industry attempts at greenwashing, this is not going to change in the foreseeable future.

It will be almost impossible for anyone to reach the required carbon footprint for us to reach net zero with even one flight a year. Sustainable Sailing is going to require us to accept that flying will need to be restricted to family emergencies only.

Our experience is that friends and family find this voluntary restriction incomprehensible. Essentially our society is living in denial regarding the impact of flying on the climate. That has been exacerbated by huge amounts of greenwashing by the airlines, with their false claims that fossil fuel free fuel is imminent and ignoring both the impact of emissions high in the atmosphere and the numbers of empty flights.

Between 1988 and 2005 Dave did a lot of flying for work, often multiple flights per month. However, we have been able to choose not to fly since summer 2005. It is hard to completely rule out flying once we start cruising around the world, but our goal is to limit ourselves to really significant family events.

As with just about everything to do with Carbon Footprints and our impact on the Climate, there is huge inequality around flying. At the time of writing we are living in Wythenshawe, Manchester, UK in a community where there are many people who have never been even 50 miles from home. Yet at the same time the use of private jets (which make commercial flights look frugal) has seen massive growth. Recently, in a single afternoon our Prime Minister made a completely unnecessary return flight to near his own home with a carbon footprint of multiple times what we can afford per person per year.

This highlights one of the key challenges around any attempt to live in a Sustainable way. Compared to the impact of the top 10% of Carbon Footprints (both individuals and corporations) the difference we can make seems small. We are not suggesting that Climate Emergency will be over if every sailor doesn't fly. The problem is too huge for most individuals to have much direct impact. Unfortunately, for decades we were sold the myth that all we had to do was change out lightbulbs. It isn't really surprising that so many people have given up, or lurched into denialism.

So what are we about? For us this is about modelling, living what we believe, trying to challenge expectations and where we can influencing others. It is absolutely not about punishing ourselves or living a joyless life - for the reality is that we have been sold so many lies about what we need and what brings joy, hope and purpose. So we make the changes that allow us to live well but with a personal footprint, cutting out flying is both essential to a sustainable carbon footprint and one of the easiest ways to make a radical cut.
