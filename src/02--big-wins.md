# Chapter 2: The big wins

We suggest starting with a few big wins when looking to be more sustainable. In a way this is the opposite of Greenwashing which works from the idea that it is easier to pretend than to make change.

So far we have eight suggestions for big wins. This follows a combination of the [low hanging fruit model](https://www.merriam-webster.com/dictionary/low-hanging%20fruit) ("the obvious or easy things that can be most readily done or dealt with in achieving success or making progress toward an objective") and the [Pareto Principle](https://en.wikipedia.org/wiki/Pareto_principle) ("The Pareto principle states that for many outcomes, roughly 80% of consequences come from 20% of causes (the "vital few").")

These are our top eight big wins:

- [Abandon New](./02-02-abandon-new.md)
- [Right Sizing](./02-04-right-size.md)
- [Sorting out your poop](./02-06-poop.md)
- [Adding more simplicity](./02-08-simplicity.md)
- [Add more DIY](./02-10-diy.md)
- [Add more time](./02-12-time.md)
- [Stop Flying](./02-14-flying.md)
- [Pareto Vegan](02-16-vegan.md)

Some gaps might surprise you, especially if you have been following our refit of Vida. We do not believe that switching to an electric motor should automatically be in the list of big wins (with some exceptions see [Motoring](./07-02-motoring.md)) nor will another of our big changes which is to go for Dyneema Rigging (see [Rigging and Sails](./06--rigging-sails.md)).
