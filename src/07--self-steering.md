# Chapter 7: Self Steering

Vida came with the original Necco self steering unit. The control panel was in poor condition (eg missing on/off switch) and the control is by setting a course by a dial which means you can't just turn on to hold the current course and tacking etc require you to first calculate the new course rather than choose the amount to turn to post or starboard. 

![Necco autopilot panel](./images/necco_panel.jpg)
![Necco autopilot drive unit](./images/necco_drive.jpg)

Our first choice is to use a form of Wind Vane self steering and a [Hydrovane](https://hydrovane.com/) is the obvious choice as most other systems require control lines to the steering wheel which would be difficult with our steering at the forward end of the centre cockpit. There isn't enough space under the aft cabin berth for the other alternative which is a [Cape Horn](http://caphorn.com) that connects internally to the steering quadrant).

However, we are ruling out the Hydrovane (at least for the present) as it is outside our budget and will be difficult to fit around the mizzen and our plans for solar panels.

Long term we would love to have a modern electronic autopilot (one with the drive unit under the deck for a long life directly working in the dry onto the steering quadrant) but these are also way outside our budget.

In deciding what to do we have found two resources particularly helpful:

- The Wave Rover Mk3 DIY self steering plans (NB it uses a trim tab on a transom mounted rudder - which we don't have so we can't ) see this YoTube video [MK3 Wind Vane [Trim Tab] Self Steering Digital DIY Plans](https://www.youtube.com/watch?v=Wcd9Y5AS6qE). The latest videos include bits about are showing Alan building a new Mk3 for his new Wave Rover boat.
- This book [John Letcher’s “Self-steering for sailing craft”](https://jesterchallenge.wordpress.com/articles/self-steering/)

## Our Plans

We want to end up with a DIY self steering system that is approximately a combination of the Wave Rover Mk3 and a Hydrovane. However, we are going to work towards this in multiple smaller steps.

### New External Emergency Rudder

We are going to add a transom hung rudder for emergency use and for use with self steering. It will look roughly like a diy version of the hydrovane rudder but with a full length tiller. We want to be able to lift this off for docking and we want it to be able to to tilt up if it hits anything. There are techniques from both Wharram Catamarans and Woods Sailing Catamarans that should help us do this ourselves.

The rudder supports will be integrated in boarding steps on our stern.

Once it is working we will be able to have cheap electronic self steering using a second hand tiller autopilot that we have bought.

### Add vane steering

We are making plans for a variation of the Wave Rover Mk3. The key changes will be:

- use a metal shaft inside the tower that the vane moves up and down (like a hydrovane). This will allow the tower to be rotated freely (not possible on the Mk3)
- try directly turning our external rudder rather than a trim tab. That will simplify things but will require a well balanced rudder, a larger wind vane and a focus on low friction everywhere (if we cannot achieve this then we know we can make the system far more powerful by getting it to turn a trim tab).
- add the ability to remotely turn the tower from the cockpit using control lines (helped by having the vertical shaft rather than the two control lines)
- add an additional horizontal arm on cranks between the tiller and the tower so that (unlike the MK3 and the Hydrovane) the tower does not need to be mounted directly above the rudder pivot (to allow us more positioning flexibility around the mizzen and solar panels)
