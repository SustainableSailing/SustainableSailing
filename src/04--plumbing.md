# Chapter 4: A Plumbing Manifesto

When we bought Vida the plumbing was a mess. We had three seacocks in the forward heads, two in the galley, three in the aft heads, two for cockpit drains and one for the engine cooling inlet (many very inaccessible). Plus an extra hole for the log impeller. All in very poor condition (although only the newest - engine cooling - was condemned by the survey). The galley waste was completely blocked by debris.
![Blocked galley waste pipe](./images/blocked-galley-waste.jpg)

All this meant we needed to make changes, we didn't want to simply update as that would mean we would still be:

* pumping raw sewage into the sea (no black holding tank which is now illegal in many places)
* draining all grey water in to the sea (starting to be illegal in some places)
* still a safety concern with (according to some articles) 50% of all boat sinkings due to skin fitting failures.
