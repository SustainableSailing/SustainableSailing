## Through Hulls

It is claimed (although I am a little suspicious that the sources are from those with an interest in selling stuff) that skin fittings with the seacocks and pipes connected to them are the most common cause of boats sinking (particularly when on a mooring or in a marina).

Draining sewage and other waste from boats is also one of the most significant ways that sailors pollute harbours and fragile ecosystems along our coastlines.

Therefore we are going to be very opinionated (again).

1. No blackwater outlets into the sea
2. No greywater outlets directly into the sea.
3. All underwater skin fittings to be protected either by standing pipes (only possible in metal boats) or by coffer dams.

On Vida we need two skin fittings with seacocks for our cockpit drains (with a centre cockpit we can't drain out through the transom which is definitely the best option). These are close to the stern tube. We are adding one salt water intake (with branches for water maker, galley sink, deck wash) in the same area. Then all seacocks and the stern tube can be encased in a coffer dam that extends to above the waterline. In theory any failure will not cause us to sink.

We are using Trudesign composite skin fittings to avoid issues with metal fittings and galvanic corrosion.

At the time of writing we are building a thrust bulkhead for an Aquadrive. For us this achieves a number of different objectives beyond the immediate alignment and vibration benefits of an Aquadrive. Our Thrust bulkhead is just forward of the two seacocks for the cockpit drains. Therefore we are going to be able to use it as for forward section of a coffer dam that will enclose the stern tube (for the propeller shaft) and the two cockpit drains. Eventually we will add a salt water intake seacock in this area (for deck wash, watermaker etc). The aim is to ensure that if a seacock, hose or the dripless seal fails the leak will be contained within bulkheads that extend to above the waterline.
