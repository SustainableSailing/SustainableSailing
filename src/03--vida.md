# Chapter 3: Introducing Vida

In the Summer of 2019 we started looking seriously for a boat that we could prepare for a live-aboard retirement in around 2024 or 2025. When looking for a boat that we could afford (which meant one that needed a lot of work doing on it) geography was important. We were not in a position where we could move to where a boat was. So either we needed to find a boat near enough to Manchester, UK or we would need to be able to sail or ship it to somewhere close. The first option was going to be much cheaper.

We looked at a number of boats around North Wales as well as scouring the Internet. We only found Vida because, out of curiosity. I increased the maximum price for the search. Then Vida showed up in a boat yard where we were already planning to look at a Samphire 26 (for less than 1/4 the price).

Initially Jane wasn't at all interested in climbing an unsecured ladder onto the largest and most expensive boat we had viewed. Fortunately, I had a quick look and then persuaded her to come aboard. It was basically love at first sight for us both.

Compared to other boats we might have been able to afford and which might have been suitable for our dreams Vida was unique in combining a proper cruising sailing boat with the centre cockpit and a semi enclosed “wheelhouse”. The aft cabin with an ensuite head accessed via a "stoop through passageway" was a huge factor.

Rival Yachts had all the features we wanted (particularly proven Ocean crossing ability, a hull shaped for big seas and a full skeg for the rudder), and we thought the centre cockpit version had a much better layout (plus there were no aft cockpit versions available in our price range). We had known that Rival  Yachts had produced a range of long distance cruising yachts that were recognised for their ability handle bad conditions, but we hadn't looked up the range enough to find one with a comfortable double bed for a live-aboard couple. In fact the Rival 38 with centre cockpit and ketch rig is one of the rarest. We believe only 5 or 6 were built. So we were incredibly fortunate to even find one for sale, let alone only 100 miles away from home (there are very few boatyards that can handle a 1.5m draught boat closer to us).

From [Sailing Today](https://www.sailingtoday.co.uk/boats/big-boat-review/used-test-rival-36/)

>Mention Rival yachts and most sailors think of fairly heavy, pretty, bulletproof affairs designed for crossing oceans in safety, with high speed not featuring particularly high on the priorities list. Rival Yachts, in its first iteration, was started by yacht designer Peter Brett in 1967 with the Rival 31, the first one of which was popped out of her mould in 1968. This was quickly followed by the slightly extended 32 and then later with the well respected 34, 41 and 38. The first 36 was launched in 1980, making it the sixth and final Rival from the company in its original form.
>
>The Rival 34 in particular had gained many admirers in serious yachting circles following Wild Rival winning the OSTAR transatlantic race on handicap in 1976. The race had been a windy one and the 34 had triumphed in the main due to an ability to plug relentlessly to windward in conditions that had caused many other crews to ease off and yachts to retire.

Our belief is that we have bought a boat that can sail adequately which means somewhat slow in light winds with performance in strong headwinds as a strong point (with a caveat that there is almost nothing written about the ketch rig as so few were built with it) (see the chapter on [Rigging and Sails](./06--rigging-sails.md) for how we are working to improve the performance using developments since the 1970's) and do so in more comfort (protected from the weather). Having a “proper” sailing boat is critically important for all three of our sustainability aims. It reduces our dependence on a diesel engine (our goal is be to end up nearly [fossil fuel free](07--fossil-fuel-light.md)). It saves money (the wind is free at point of use, yes we know gear/sails wear out but it is nothing compared to the cost of fuel). We find Sailing so much more pleasurable than motoring so focusing on this is key to sustaining our emotional well being.

With their “traditional” shape there isn’t a huge amount of volume in a Rival boat compared to many modern designs. So the 38 is the smallest which allows an aft cabin with “walkway” (more accurately a stoop & squeeze) access from the main cabin. That gives us a double bed (with the added and unexpected bonus of an en-suite head) that is separated from the rest of the boat. That means it can be kept further away from the salt and chaos during passage making so that it is more pleasant when at anchor (and an aft cabin is a much more quiet and comfortable place than a v-berth at the bow). It also helps during a refit as it is easier to have a place to sleep which is not filled with dust and mess. Finally, it gives that opportunity for a little more personal space whether that is when I stay up after Jane goes to bed or when she gets up before me.

What we don’t get is the huge cockpit so key to modern designs designed for high speed sailing (providing you avoid storms and loading the boat too much) and entertaining large numbers of people when moored stern to a quay in the Mediterranean (not something on our wish list anyway). The galley is also rather small compared to more modern designs (culinary expectations were perhaps lower in the 1970’s).

So what sort of condition is a 1977 year old boat in? Does that explain why she was for sale at a price we could afford?

The most obvious problem was that the toerail (in this case a rubber capping over an aluminium strip fixed on top of the bulwark that is the joint between the hull and the deck) is missing.

The survey showed the hull and deck to be in great condition. There were very few places needing any work (the davits have caused some crazing in the deck as has one stay). The standing rigging was also ok for a couple more years. However, all the running rigging needed replacing and the mainsail furling (added to the aft edge of the mast) had a lot of problems. The anchor, chain and manual windlass were all in very poor condition.

Inside the main need was for a huge clear out and clean. 42 years of stuff and quite a lot of mildew. All the cushions have passed the end of their usable life. The entire gas system (bottles, regulator, flexible hose, fixed pipe, tap to cooker, flexible hose and the cooker itself) were condemned. What we did not realise was how much all the cabin windows and hatches had been leaking (the foam backed vinyl headlining had soaked up water and moved it away from the leaks).

The engine was only a few years old and seemed sound. It needed a new seacock, the stern gland and cutlass bearing needed work, one oil filter was leaking.

Electrically, the system was essentially original. It needed new batteries, the grounding wasn’t complete, the autopilot needed a new on/off switch, the VHF was ancient, there was no chart plotter or AIS. The fridge and the hot air heating did’t work.

We thought the timber throughout was in pretty good condition with just some bits of mould, water damage and corroded screws.

We noticed that the headlining was coming away in some lockers but seemed adequate in the main cabins (we were very wrong).

The toilets were working but there were no holding tanks so they both direct discharge into the sea. The main heads had a sliding basin which was pretty dirty, didn't slide much at all and the taps were quite corroded.

So we thought we got a boat where the fundamentals were good (hull, deck, masts, engine). Naively we  thought not too much needed to be done for her to be able to be launched. Lots to do to make her nice to live on but that can be done in stages. Nothing where things have been updated in ways we don’t like (apart from the mainsail furling, but that was now worn out anyway).

From this view (mostly written just after purchase) things changed quite quickly for three main reasons:

* we discovered the extent of the leaks that have required us remove all the headlining, replace all the windows, remove very leaky dorade boxes, and rebuild two deck hatches.
* we discovered that we needed to take the main mast down to have the main mast furling system removed, it had been riveted to the aft of the mast.
* we discovered issues with the chainplates for both masts that were potentially very expensive to fix.
* COVID 19 arrived and in total various lockdowns and restrictions meant that we were unable to get to Vida for a total of about 9 months.
