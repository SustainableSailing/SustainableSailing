## Pareto Vegan

I just invented this :-)

Most studies (apart from those from vested interests) show that a Vegan diet has a significantly lower carbon footprint than the alternatives ([The Carbon Foodprint of 5 Diets Compared](https://shrinkthatfootprint.com/food-carbon-footprint-diet/)). However, it seems a huge challenge for many (for example we absolutely love cheese). It can also be difficult when travelling and eating out in many parts of the world.

Therefore, I suggest adding the [Pareto Principle](https://en.wikipedia.org/wiki/Pareto_principle) ("The Pareto principle states that for many outcomes, roughly 80% of consequences come from 20% of causes (the "vital few").") to the mix.

If we follow a mostly Vegan diet then it might be possible to get 80% of the benefit while avoiding 80% of the difficulties. With this meat would become a rare "treat" when eating out (and then choices made according to locality eg no Australian beef in the UK). We would replace a lot of dairy (which is difficult to store on passage) with vegan alternatives (home made oat or soya milk for example). We could eat fish that we catch (only fishing where it can be considered sustainable, avoiding fragile habitats). Our carbon footprint would be significantly reduced at minimal lifestyle impact.

Of course this assumes a Vegan diet is being followed for environmental rather than ethical/animal welfare reasons.
