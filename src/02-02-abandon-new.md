## Abandon New

The single biggest choice for more sustainability is not to buy a new boat. New boats create a huge new carbon footprint and that is true of even the "greenest" options. That carbon footprint will probably never be recovered through efficiency savings.

It seems very normal to kid ourselves that buying a new boat (especially remembering that new boats are typically larger and rarely local) is a sustainable option because they come with the latest in solar technology. However, the footprint of the materials, of transport, manufacturing, flights to visit and/or collect the boat is going to be enormous.

Contrast this with a used boat, especially if it is an older boat that is being saved from disposal or abandonment. The entire carbon footprint from production is now a sunk cost, it has happened and your purchase does not change it at all.

Refitting almost any project boat is going to require a fraction of the resources of a new build. Plus you are putting off (potentially almost indefinitely) the waste disposal issues when a boat can no longer be saved.

Typically, there is a lot more that can be done to update the interior and systems of a project boat than we expect. In part this is because so many of the refits have been done on a tiny budget. It is perfectly possible to end up with an excellent, attractive, safe, comfortable cruising yacht from an older boat.
