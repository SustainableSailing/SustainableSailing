## Right sizing

The surveys that Jimmy Cornell has been doing for decades show a trend towards larger yachts and more multihulls ([according to Practical Boat Owner](https://www.pbo.co.uk/boats/jimmy-cornell-boat-survey-sailing-changed-74649) from 39.8ft in 1978 to 49ft in 2022). This trend has been pushed by industry marketing and the hype of large YouTube channels they are continually spreading the message that we need bigger, faster and more spacious boats for safety (Sailing la Vagabond for example).

This large multihull trend is particularly challenging to follow if you look for a used boat because other than large racing multihulls there are very few used catamarans and trimarans that are in the 15m (50 foot) size range. Some are converting large racing monohulls to cruising (eg Sailing NV and The Duracell Project) but they are far beyond an "attainable" cruising budget and they have significant challenges such as draught (US: draft).

This was worth noting in the [PBO article](https://www.pbo.co.uk/boats/jimmy-cornell-boat-survey-sailing-changed-74649):

```admonish quote
The most satisfied owners were in the 35-40ft range, of which there were a total of 25 boats, with average crews of either two or three. It was interesting there were many more complaints about size both from skippers of boats under 35ft (12) and those over 45ft (16).
```

### Size for living aboard

When we look at the Sustainable footprint of living aboard a boat we believe that there is a sweet spot, which is around that 35-40ft range noted by Jimmy Cornell.

#### Too small?

There are practicalities that mean many of the smallest boats used for passage making or cruising are not great live aboard boats (especially for couples like us). Looking at the [Wave Rover 650](https://www.youtube.com/@SailingWaveRover) for example we see a boat designed for Ocean passages but not for living aboard where significant time will be spent at anchor and where you would have most of your processions with you.

We believe that a boat that requires you to have another home to spend time in (eg where you can stand up and store stuff) fails on Sustainability. Maintaining two homes and travelling, probably by plane, between them, is more expensive and has a high carbon footprint.

So a sustainable sailing boat will as small as possible while providing enough space for it to be your only home all year round and especially allow you to avoid the need to fly elsewhere for a break, or for your extra stuff. Any flights will easily increase your carbon footprint (and your financial cost) by more than a slightly larger boat.

The way the size of the boat translates into usable space and weight capacity is also important for living aboard. We really value having a separate aft cabin as Jane goes to bed and then wakes much earlier than me. So it allows one of us to be doing things in the saloon while the other sleeps. Similarly for us, having two heads compartments means we can disturb each others sleep less.

#### Too large?

The costs of maintenance/repairs/refitting boats tends to be proportionate to displacement not length (with the proviso that you compare similar displacement/length ratios - a light high performance 30ft boat will need more expensive materials and systems than a heavy displacement 35 ft boat). This is identical for financial and carbon footprint costs. So there is a direct relationship between size (length and displacement) and sustainability.

As soon as you need to rely on systems such as electric winches for your sails the costs rocket (and so will time spent on maintenance). As the costs rise the carbon footprint does too.

Once cruising, the cost of moorings, marina berths, haulouts etc all increase with length. In the case of multihulls we see examples of even higher, unexpected costs such as [Sailing Parlay Revival](https://www.youtube.com/c/parlayrevival) (a large Lagoon catamaran) who had to pay to go through the Panama Canal an extra two times so they could get hauled out for major repairs (only available on the Atlantic side).

### Size issues during refitting

Having watched many people doing refits and now 4 years into ours there are some significant issues related to Sustainability and boat size during the refit project stage.

Obviously, as the saying goes "*size matters*" ;-) Generally, smaller boats are going to be easier and quicker to refit.

However, there is also the issue of where you live while doing the refit. Some people will be working on the refit full-time and of those many (at the lower range of financial security) will need to live on the boat. Others may live near enough to the boat to live in a different home. Smaller boats (or big refits where it is cost effective) can be moved to where you live for the refit.

We do not have space for the boat where we live (and it is a house provided by Dave's job rather than our own). We cannot afford to rent or buy accommodation close to the boat (and there are no suitable boatyards much closer to home than 100 miles where Vida has been since we bought her).

Therefore, whenever we go to work on Vida we need to be able to be able to sleep on her whatever tasks are in progress. For us a significant benefit has been that Vida has an aft cabin. With 3 cabins (forecabin, saloon and aft cabin) we have been able to have a sleeping space, big refit tasks space and a storage "garage" whichever area we have been working on. So it has been easier to keep our sleeping area more isolated from fibreglass dust.

A smaller boat would have been less comfortable during our often short weekend visits without that separation and the inconvenience would have slowed us down. A bigger boat would be outside our budget for purchase, refit and cruising.

### Taking the ground

One huge potential for reducing the cost of refits and cruising is a boat that can dry out safely and do so reliably and frequently. There are three most common ways for this:

* A multihull. A great example of what is possible is shown in the videos from "Living On The Ocean" such as [Extending her Transom on the Sandbank. Trimaran Dawn 016](https://www.youtube.com/watch?v=rB2Gdh6zM64)
* a bilge keel (or triple keel) such as Karl [White Spot Pirates:
GROUNDED SAILBOAT: How I beached my sailboat on purpose - UNTIE THE LINES IV #51](https://www.youtube.com/watch?v=6eivWxeu-IM)
* A fully lifting keel such as on a Garcia, Boreall, Allures. All so far outside our budget that we have no opinion about them. There are few designs old enough to be within out budget that combine lifting keels with suitability for ocean cruising. 

Of these our personal preference would be a catamaran as you can more reliably dry out level and do so in shallower waters giving much longer working times. The small number of cheap used catamarans suitable for ocean cruising is the key limiting factor (Wharrams are the obvious option, providing you can find one that was built to a high standard with Epoxy).
