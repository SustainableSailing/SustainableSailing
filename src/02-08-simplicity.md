## Adding more simplicity

```admonish tip
If you don't have it, then it can't break and it won't cost you anything to maintain it.
```

```admonish tip
If you don't have it, then it won't take any space or add weight to your boat.
```

```admonish tip
If you don't have it, then it has not added to your Carbon Footprint.
```

We struggle with aspects of simplicity. Dave loves tech and gadgets. Jane loves an infinite range of sewing related crafts. We are slow to adopt simple, low tech "solutions" to challenges. Neither of us finds being tidy and minimalist easy.

However, we have watched many YouTube videos of people spending huge amounts of time and money fixing or maintaining "stuff" on their boats and are fully convinced that this is an area where we will experience huge benefits if we can learn to be better at Simplicity.

Simplicity supports both Sustainability (reduced resources used to purchase/install/maintain) and Safety (fewer things to fail, reduced dependencies, easier to repair).

We believe that not enough weight is given to the multiplier effects of gadgets on boats. For example it is becoming more and more common to add appliances such as Freezers and Washing Machines. We haven't seen any boat with these that hasn't needed to add a generator. Suddenly the maintenance cost and workload has increased significantly (watching [Sailing SV Delos](https://www.youtube.com/@svdelos) is great proof of this).

Here we can see how things can fit together. A [Pareto Vegan](02-16-vegan.md) diet will (with other benefits too) dramatically reduce the need for a Freezer. The way you choose to do [Laundry](./11-02-laundry.md) can make a massive difference to your time and costs especially if Laundrettes and an electric washing machine can be avoided.

Switching to windvane self steering from an electric autopilot has the potential for a range of multiplier benefits but it does mean embracing a particular low impact cruising style with less motoring, slower passages and a less controlled timetable. We are hoping for a halfway option where we will use a tillerpilot on our windvane tiller rather than install an expensive electric/hydraulic autopilot to work with our wheel steering. A full electric autopilot has many multipliers caused by the energy consumption and the need for spares if you depend on a working system. See the [SailLife YouTube channel](https://www.youtube.com/@SailLife) for some of the challenges this presents.

The galley is another area where the trend has been towards complexity with implications for cost, reliability and sustainability. During the early days of our refit, when ashore with very limited galley space, we bought an electric "InstantPot". It was very convenient, we could prepare a vegetable stew at home in the pot and just plug it in when we got to the boat. Unfortunately, it has now failed after only 4 years. The latest trend is to offer even more cooking modes (pressure cooker, slow cooker, air fryer combined). However, the key problems we have had (LED display failing, thin pan bending so the seal fails, thin pan with coating losing non stick and impossible to clean) will only be worse with these trends. So instead we have gone more "old school" with a Stainless Steel "traditional" stove top pressure cooker that works on our induction hob. Comes with a 25 year guarantee and will work on any hob type (so we can use even if we don't have electricity to spare). More than that, it can be used as a regular saucepan which saves space & weight, it cooks at higher pressure and so faster, finally it is much easier to clean (saves water and time).

The same is true of coffee making, we have seen boats going for coffee pod machines which have plenty to break, poor sustainability and high energy use. Instead we get better coffee more reliably, at lower cost by buying roasted beans in bulk. We make our coffee using a hand grinder (I treated myself to a [1zPresso Ultra](https://1zpresso.coffee/k-ultra/)), normal stove top kettle and an [Aeropress](https://www.aeropress.co.uk/) (I did go fancy with a digital scale/timer - a [Timemore Nano](https://us.timemore.com/collections/coffee-scale/products/timemore-black-mirror-nano-espresso-coffee-weighing-timing-black) - which isn't really necessary). For the technique see [this video from the brilliant James Hoffman](https://www.youtube.com/watch?v=j6VlT_jUVPc). We will need a different, safer solution for when we are on passage, one that does not require pouring boiling water onto a stack of things balanced on each other. That will probably be a basic electric filter coffee machine with a thermos flask (conveniently what we already have at home), that way you pour cold water in and the flask is sealed with a button press to pour. Yes this means we can't make a proper Espresso. However, instead of a poor quality semi Espresso we get a really high quality Americano when in harbour (and with the microwave and a small French Press we can make a pretty good latte). At sea an espresso seems overkill, a filter coffee makes a lot more sense (a longer drink to warm yourself up with).

In our chapter on [Rigging and Sails](./06--rigging-sails.md) we explore going for Dyneema rigging which allows for far more simplicity (plus [Add more DIY](./02-10-diy.md) and cost savings - especially if, like us, your standing rigging already needs replacing).

[Sorting out your poop](./02-06-poop.md) is another great simplification when the "industry trend" (electric flush toilets) is adding more complexity, more cost and higher energy usage.

Simplifying [Plumbing](./04--plumbing.md) and in the process increasing safety by getting rid of [through hull fittings](./04-02-through-hulls.md) is another win-win as it increases safety and reduces pollution.
