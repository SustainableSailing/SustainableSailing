# Supporting Sustainable Sailing

If you would like to support writing this book and our Sustainable Sailing project then we have a couple of options:

## Donate using Ko-Fi

We have a [donation page using Ko-Fi](https://ko-fi.com/sustainablesailing) where it is simple to make a one off or regular donation to support Sustainable Sailing.

## Buy Merchandise

We have cool t-shirts, hoodies and bags in [our online shop](https://sustainablesailing.teemill.com/)
