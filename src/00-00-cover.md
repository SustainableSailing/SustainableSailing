# <div style="text-align: center;">Sustainable Sailing</div>

<div style="text-align: center;">Sustainable includes:</div>

<div style="text-align: center;">Environmental (low carbon footprint, low pollution, low impact)</div>
<div style="text-align: center;">Health (low stress, good exercise, good diet etc)</div>
<div style="text-align: center;">Financial (manageable costs, enough income)</div>

![Sustainable Sailing Logo](./images/SSlogoChosenHR.jpg)

&copy; Dave and Jane Warnock 2023
