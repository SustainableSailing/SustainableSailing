# Introduction

Sustainable Sailing is both our vision and our journey. By "our" we mean Jane and Dave Warnock. We have been married over 35 years and are preparing for the next "stage" of our lives, one focused on Sustainability while sailing around about the world. For nearly 20 years Dave has worked as a Methodist Minister, that means we have lived in three different houses provided by the Methodist Church. We have had no control over these houses and they have all had high carbon footprints with: little insulation, old fashioned heating systems, no solar panels etc. We have tried to reduce our Carbon Footprint over the years but the houses have been the limiting factor. 

We have enjoyed sailing together since we first got married but had a break for over 10 years as our teenage sons didn’t share that enjoyment. So now we are combining our passion for living sustainably with sailing and a little planning for retirement.

This journey started with the purchase of Vida, a 1977 Rival 38 Centre Cockpit sailing boat. Together we are looking to a future for ourselves which is:

* Sustainable – Our total environmental impact
* Sustainable – Our health and well-being
* Sustainable – Our finances

  
As so often there is a long and a short story.

So here is the short version. We were prompted to look towards our retirement. We realised that one day living on a sailing boat might be possible. We looked at a few boats to check the dream. Vida appeared on a list and changed our expectations and our timetable. She motivated us to get on and sell a plot of land (our plans for that had fallen through). We made an offer, had a survey and bought her, signing the paperwork on Friday 23th August 2019, when we arrived to meet the previous owner and spend the bank holiday weekend on Vida, getting started on the jobs.


To support us so that we can update this book and move towards more Sustainability see [Supporting Sustainable Sailing](95--supporting.md)
