# Sustainable Sailing

Sustainable Sailing is both our vision and our journey. By "our" we mean Jane and Dave. We have been married 35 years and are starting to plan for a different future. Currently we live in a house provided for Dave’s job which limits our options for Sustainable Living. We have enjoyed sailing together since we first got married but had a break for over 10 years as our teenage sons didn’t shared that enjoyment. So now we are combining the our passion for living sustainably with sailing and a little planning for retirement.

This journey started with the purchase of Vida, a 42-year-old Rival 38 Centre Cockpit sailing boat. Together we are looking to a future for ourselves which is:

* Sustainable – Our total environmental impact
* Sustainable – Our health and well-being
* Sustainable – Our finances

As so often there is a long and a short story.

So here is the short version. We were prompted to look towards our retirement. We realised that one day living on a sailing boat might be possible. We looked at a few boats to check the dream. Vida appeared on a list and changed our expectations and our timetable. She motivated us to get on and sell a plot of land (our plans for that had fallen through). We made an offer, had a survey and bought her, signing the paperwork on Friday 23th August 2019, when we arrived to meet the previous owner and then spend the bank holiday weekend on Vida, getting started on the jobs.
